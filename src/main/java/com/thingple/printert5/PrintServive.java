package com.thingple.printert5;

import android.util.Log;

import com.sdk.bluetooth.android.BluetoothPrinter;

/**
 *
 * Created by lism on 2017/10/26.
 */
public class PrintServive {

    private BluetoothPrinter printer;

    private boolean available;

    public PrintServive() {
        try {
            BluetoothPrinter printer = new BluetoothPrinter("T5");
            printer.setCurrentPrintType(BluetoothPrinter.PrinterType.T5);
            this.printer = printer;
        } catch(Exception e) {
            Log.e(getClass().getName() + "#PrintServive", "创建打印机失败", e);
        }
    }

    public void print(PrintHandler printHandler) {

        if (!available) {
            return;
        }
        if (printHandler != null) {
            printHandler.doPrint(printer);
            close();
        }
    }

    public String open() {
        if (printer == null) {
            available = false;
            return "请连接蓝牙打印机";
        }
        if (printer.open() != 0) {
            available = false;
            return "打印机不可用";
        }
        available = true;
        return null;
    }

    public void close() {
        if (this.printer != null) {
            if (this.printer.isConnected()) {
                this.printer.close();
            }
            this.printer = null;
        }
    }
}