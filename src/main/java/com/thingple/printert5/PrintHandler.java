package com.thingple.printert5;

import com.sdk.bluetooth.android.BluetoothPrinter;

/**
 *
 * Created by lism on 2017/10/26.
 */

public interface PrintHandler {

    void doPrint(BluetoothPrinter printer);
}
