package com.thingple.printert5.sample;

import android.util.Log;

import com.thingple.printert5.PrintServive;

/**
 *
 * Created by lism on 2017/10/26.
 */

public class TestTheHandler {

    public void print() {
        Report report = new Report();
        report.title = "客户签收单";
        report.plateNumber = "沪Z1034";
        report.plan = "44333";
        report.customerName = "测试客户";
        report.customerContact = "测试客户工人";
        report.customerAddress = "测试地址";
        report.operation = "签收";
        report.cylinders.put("氮气", 19);
        report.cylinders.put("氢气", 26);

        report.anomalies.add("00002345");

        PrintServive printer = new PrintServive();
        String error = printer.open();
        if (error == null) {
            ReportPrintHandler handler = new ReportPrintHandler(report);
            printer.print(handler);
            printer.close();
        } else {
            Log.d(getClass().getName() + "#print", error);
        }
    }
}
