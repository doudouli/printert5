package com.thingple.printert5.sample;

import com.sdk.bluetooth.android.BluetoothPrinter;
import com.thingple.printert5.PrintHandler;

import java.util.Date;

/**
 *
 * Created by lism on 2017/10/26.
 */

class ReportPrintHandler implements PrintHandler {

    private Report report;

    ReportPrintHandler(Report report) {
        this.report = report;
    }

    @Override
    public void doPrint(BluetoothPrinter printer) {

        try {

            printer.setPrinter(10, 5);

            String edtContext = report.title + "\n";
            printer.send(edtContext.getBytes("GB18030"));

            printer.setPrinter(4); // Add new line
            //printer.setPrinter(10, 80);

            printer.send(new Date().toString());
            printer.setPrinter(4);

            byte[] overprinting = { 0x1B, 0x45, 0x01 };
            printer.send(overprinting);

            byte[] charLarge = { 0x1B, 0x4D, 17 };
            printer.send(charLarge);

            String truckname = "卡车: " + report.plateNumber;
            printer.send(truckname.getBytes("GB18030"));
            printer.setPrinter(4);
            String dev = "";
            dev += "交付计划号:\n" + report.plan;
            printer.send(dev.getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send("客户名称:\n".getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send(report.customerName.getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send("联系人:\n".getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send(report.customerContact.getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send("地址:\n".getBytes("GB18030"));
            printer.setPrinter(4);

            printer.send(report.customerAddress.getBytes("GB18030"));
            printer.setPrinter(4);


            String print = "";
            print += "操作节点:\n";
            print +=  report.operation + "\n";
            print += "气瓶数: ";
            print +=  report.total() + "\n";

            printer.send(print.getBytes("GB18030"));

            for(String gas : report.cylinders.keySet()) {
                String content = gas + report.cylinders.get(gas) + "\n";
                printer.send(content.getBytes("GB18030"));
            }

            if(!report.anomalies.isEmpty()) {
                printer.send( "异常:\n".getBytes("GB18030"));
                for (String tag : report.anomalies) {
                    String ref = tag + "\n";
                    printer.send(ref);
                }
            }

            printer.setPrinter(4);
            printer.send("       签名:\n".getBytes("GB18030"));

            // Add new line
            printer.setPrinter(4);
            printer.setPrinter(4);
            printer.setPrinter(4);

            printer.setPrinter(4);
            printer.setPrinter(4);
            printer.setPrinter(4);

            printer.setPrinter(4);
            printer.setPrinter(4);
            printer.setPrinter(4);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
