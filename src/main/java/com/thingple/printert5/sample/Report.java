package com.thingple.printert5.sample;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report implements Serializable {
    public String title;
    public String plateNumber;
    public String plan;
    public String customerName;
    public String customerContact;
    public String customerAddress;
    public String operation;
    // gas_type + "  : " + number
    public Map<String, Integer> cylinders = new HashMap<>();

    // tag.getReference
    public List<String> anomalies = new ArrayList<>();

    public int total() {
        int n = 0;
        if (cylinders != null) {
            for (String gas : cylinders.keySet()) {
                n = n + cylinders.get(gas);
            }

        }
        if (anomalies != null) {
            n = n + anomalies.size();
        }
        return n;
    }
}
